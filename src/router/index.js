import { createRouter, createWebHistory } from 'vue-router'
import Layout from '@/layouts/layout.vue'
import PipelineOverview from '@/views/PipelineOverview.vue'
import InteractionEffect from '@/views/InteractionEffect.vue'
import IntegratedManagement from '@/views/IntegratedManagement.vue'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'layout',
      component: Layout,
      redirect: '/PipelineOverview',
      children: [
        {
          path: '/PipelineOverview',
          name: 'PipelineOverview',
          component: PipelineOverview,
        },
        {
          path: '/InteractionEffect',
          name: 'InteractionEffect',
          component: InteractionEffect,
        },
        {
          path: '/IntegratedManagement',
          name: 'IntegratedManagement',
          component: IntegratedManagement,
        },
      ],
    },
  ],
})

export default router
