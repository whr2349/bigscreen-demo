```
my-vue-app/
├── public/                 # 静态资源
│   └── ...                 # 静态文件
├── src/
│   ├── assets/             # 图片、字体等
│   │   └── ...             # 资源文件
│   ├── components/         # 公共组件
│   │   └── ...             # 组件文件
│   ├── layouts/            # 布局组件
│   │   └── ...             # 布局文件
│   ├── router/             # 路由配置
│   │   └── index.js        # 路由配置文件
│   ├── store/              # 状态管理
│   │   └── index.js        # 状态管理配置文件
│   ├── styles/             # 全局样式
│   │   └── ...             # 样式文件
│   ├── views/              # 页面组件
│   │   └── ...             # 页面文件
│   ├── App.vue             # 根组件
│   └── main.js             # 入口文件
├── .eslintrc.js            # ESLint配置
├── .prettierrc             # Prettier配置
├── package.json
└── ...
```
