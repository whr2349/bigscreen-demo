// uno.config.js
import { defineConfig } from 'unocss'
import { presetUno, presetAttributify } from 'unocss'
import presetRemToPx from '@unocss/preset-rem-to-px'

export default defineConfig({
  presets: [
    presetUno({
    }),
    presetRemToPx({
      baseFontSize: 4,
    }),
    presetAttributify()
  ]
  // 可以在这里添加更多配置选项
})
